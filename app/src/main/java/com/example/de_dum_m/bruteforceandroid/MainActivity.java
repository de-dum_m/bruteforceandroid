package com.example.de_dum_m.bruteforceandroid;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends Activity {

    private static boolean haveURL = false;
    private static boolean haveUsernameItem = false;
    private static boolean havePasswordItem = false;
    private static boolean haveAccessDeniedString = false;
    private static String userFile;
    private static String passFile;
    private static SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.z
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            EditText editText = (EditText) rootView.findViewById(R.id.url_text);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        haveURL = true;
                        Button button = (Button) rootView.findViewById(R.id.start_attack);
                        button.setEnabled(true);
                    }
                    else {
                        Button button = (Button) rootView.findViewById(R.id.start_attack);
                        button.setEnabled(false);
                    }
                }
            });
            editText = (EditText) rootView.findViewById(R.id.login_failed_string);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0 && havePasswordItem && haveUsernameItem) {
                        haveAccessDeniedString = true;
                        if (haveUsernameItem && haveAccessDeniedString) {
                            Button button = (Button) rootView.findViewById(R.id.start_attack);
                            button.setEnabled(true);
                        }
                    }
                    else {
                        haveAccessDeniedString = false;
                        Button button = (Button) rootView.findViewById(R.id.start_attack);
                        button.setEnabled(false);
                    }
                }
            });
            editText = (EditText) rootView.findViewById(R.id.username_form);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        haveUsernameItem = true;
                        if (havePasswordItem && haveAccessDeniedString) {
                            Button button = (Button) rootView.findViewById(R.id.start_attack);
                            button.setEnabled(true);
                        }
                    }
                    else {
                        haveUsernameItem = false;
                        Button button = (Button) rootView.findViewById(R.id.start_attack);
                        button.setEnabled(false);
                    }
                }
            });
            editText = (EditText) rootView.findViewById(R.id.password_form);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        havePasswordItem = true;
                        if (haveUsernameItem && haveAccessDeniedString) {
                            Button button = (Button) rootView.findViewById(R.id.start_attack);
                            button.setEnabled(true);
                        }
                    }
                    else {
                        havePasswordItem = false;
                        Button button = (Button) rootView.findViewById(R.id.start_attack);
                        button.setEnabled(false);
                    }
                }
            });
            Spinner spinner = (Spinner) rootView.findViewById(R.id.protocol_spinner);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.http_form_options);
                    Button button = (Button) rootView.findViewById(R.id.start_attack);
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics()), ViewGroup.LayoutParams.WRAP_CONTENT);
                    if (position == 1) {
                        layout.setVisibility(View.VISIBLE);
                        lp.addRule(RelativeLayout.BELOW, R.id.http_form_options);
                        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        button.setLayoutParams(lp);
                        button.setEnabled(false);
                    } else {
                        layout.setVisibility(View.INVISIBLE);
                        lp.addRule(RelativeLayout.BELOW, R.id.protocol_port_layout);
                        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        button.setLayoutParams(lp);
                        button.setEnabled(true);
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
            Button button = (Button) rootView.findViewById(R.id.start_attack);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sharedPreferences = getActivity().getSharedPreferences("com.example.de-dum_m.bruteforceandroid", Context.MODE_PRIVATE);
                    userFile = sharedPreferences.getString("UserFile", null);
                    passFile = sharedPreferences.getString("PassFile", null);
                    if (userFile == null || passFile == null) {
                        Context context = getActivity().getApplicationContext();
                        CharSequence text = "Please specify a user and password list in the settings";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        EditText editText = (EditText) rootView.findViewById(R.id.username_form);
                        String usernameItem = editText.getText().toString();
                        editText = (EditText) rootView.findViewById(R.id.password_form);
                        String passwordItem = editText.getText().toString();
                        editText = (EditText) rootView.findViewById(R.id.login_failed_string);
                        String accessDenied = editText.getText().toString();
                        Spinner spinner = (Spinner) getActivity().findViewById(R.id.protocol_spinner);
                        String protocol = spinner.getSelectedItem().toString();
                        editText = (EditText) getActivity().findViewById(R.id.url_text);
                        String URLorIp = editText.getText().toString();
                        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
                        Integer threadCount = Integer.valueOf(sharedPreferences.getString("pref_threads", "10"));
                        MainAttack bruteForce = new MainAttack(protocol, userFile, passFile, URLorIp, threadCount, new MyLogger(getActivity()), usernameItem, passwordItem, accessDenied);
                        AsyncAttack asyncAttack = new AsyncAttack();
                        asyncAttack.execute(bruteForce);
                    }
                }
            });
            return rootView;
        }
    }

    protected static class AsyncAttack extends AsyncTask<Object, Void, Void> {
        @Override
        protected Void doInBackground(Object... params) {
            MainAttack bruteforce = (MainAttack) params[0];
            bruteforce.startAttack();
            return null;
        }
    }
}
