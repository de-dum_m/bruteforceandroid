package bruteforce;

class GetArgs {
    private String		userFile;
    private String		passFile;
    private String      protocol;
    private Integer		turnMethod = 1;

    private String victimURL;
    private int victimPort = 80;

    public GetArgs(String[] args) {
        for (int i = 0; i < args.length; i++)
            if (args[i].equals("--user_list") && i < args.length - 1)
                userFile = args[++i];
            else if (args[i].equals("--pass_list") && i < args.length - 1)
                passFile = args[++i];
             else if (args[i].equals("--turn_method") && i < args.length - 1)
                turnMethod = (int) Integer.parseInt(args[++i]);
            else if (args[i].equals("--port") || args[i].equals("-p") && i < args.length - 1)
                victimPort = Integer.parseInt(args[++i]);
            else if (args[i].equals("--protocol") || args[i].equals("-P") && i < args.length -1)
                protocol = args[++i];
            else
                victimURL = args[i];

        if (passFile == null)
            throw new IllegalArgumentException("Missing password list");
        else if (userFile == null)
            throw new IllegalArgumentException("Missing user list");
        else if (turnMethod < 0 || turnMethod > 1)
            throw new IllegalArgumentException("Specified turn method does not exit. 0 to loop on users, 1 for password [default]");
        else if (victimPort < 0)
            throw new IllegalArgumentException("Specified port cannot be negative");
        else if (victimURL == null)
            throw new IllegalArgumentException("Need to specify a victim's URL or IP");
        else if (protocol == null)
            throw new IllegalArgumentException("Missing protocol specification");

    }

    public String getUserFile() {
        return userFile;
    }

    public String getPassFile() {
        return passFile;
    }

    public String getVictimURL() {
        return victimURL;
    }

    public int getVictimPort() {
        return victimPort;
    }

    public String getProtocol() {
        return protocol;
    }
}

















