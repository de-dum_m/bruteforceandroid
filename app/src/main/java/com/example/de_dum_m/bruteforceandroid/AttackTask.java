package com.example.de_dum_m.bruteforceandroid;

import com.example.de_dum_m.bruteforceandroid.drivers.Driver;

import java.util.concurrent.Callable;

/**
 * Created by de-dum_m on 30/09/14.
 */
public class AttackTask implements Callable<String[]> {
    private Driver driver;
    private String username;
    private String password;
    private MyLogger logger;

    public AttackTask(Driver driver, String username, String password, MyLogger logger) {
        this.driver = driver;
        this.username = username;
        this.password = password;
        this.logger = logger;
    }

    public String[] call() throws Exception {
        logger.v("Testing " + username + ":" + password);
        try {
            if (driver.tryCredentials(username, password) == 200) {
                return new String[]{username, password};
            } else
                return null;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
