package com.example.de_dum_m.bruteforceandroid;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

/**
 * Created by de-dum_m on 08/10/14.
 */
public class MyLogger {

    private SharedPreferences sharedPreferences;
    private int debugLevel;
    private TextView textView;
    private Activity activity;

    public MyLogger(Activity activity) {
        textView = (TextView) activity.findViewById(R.id.logging_box);
        textView.setText("");
        textView.setMovementMethod(new ScrollingMovementMethod());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
        debugLevel = Integer.valueOf(sharedPreferences.getString("pref_debug_level", "10"));
        this.activity = activity;
    }

    public void v(final String log) {
        if (debugLevel >= 0) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.append(log + "\n");
                }
            });
        }
    }

    public void d(final String log) {
        if (debugLevel >= 1) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.append(log + "\n");
                }
            });
        }
    }

    public void m(final String log) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.append(log + "\n");
            }
        });
    }
}
