package com.example.de_dum_m.bruteforceandroid;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by de-dum_m on 30/09/14.
 */
public class UserPassFetcher {
    private List<String> userList = new ArrayList<String>();
    private List<String>	passList = new ArrayList<String>();
    private boolean		userFileEnded = false;
    private boolean		passFileEnded = false;
    private BufferedReader	userBuff;
    private BufferedReader	passBuff;
    private int			userListPosition = 0;
    private int			passListPosition = 0;
    static private String username;
    private String password;

    int offset = -1;

    public UserPassFetcher(String userFile, String passFile) throws IOException {
        try {
            userBuff = new BufferedReader(new FileReader(userFile));
            passBuff = new BufferedReader(new FileReader(passFile));
        }
        catch (IOException e) {
            throw new IOException(e);
        }
    }

    public long count() {
        return userList.size() * passList.size();
    }

    public long getProgress() {
        return passList.size() * userListPosition + passListPosition;
    }

    public String[] getUsernamePasswordPair() {
        if (username == null && userListPosition < userList.size())
            username = userList.get(userListPosition++);
        if (passListPosition > passList.size() -1) {
            passListPosition = 0;
            if (userListPosition < userList.size() - 1)
                username = userList.get(userListPosition++);
            else
                username = null;
        }
        password = passList.get(passListPosition++);
        return new String[]{username, password};
    }

    public void loadAllUsersAndPasswords() throws IOException {
        try {
            while (!passFileEnded) {
                getNewPass();
            }
            while (!userFileEnded) {
                getNewUser();
            }
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public void getNewUser() throws IOException{
        String result = null;

        if (!userFileEnded && userBuff != null) {
            try {
                result = userBuff.readLine();
            }
            catch (IOException e) {
                throw new IOException(e);
            }
            if (result == null) {
                userFileEnded = true;
            } else
                userList.add(result);
        }
    }

    public String getNewPass() {
        String result = null;

        if (!passFileEnded && passBuff != null) {
            try {
                result = passBuff.readLine();
            }
            catch (IOException e) { return null; }
            if (result == null) {
                passFileEnded = true;
            } else
                passList.add(result);
        }
        return result;
    }

    public void setPassListPosition(int position) {
        passListPosition = position;
    }
    public void setUserListPosition(int position) {
        userListPosition = position;
    }
}
