package com.example.de_dum_m.bruteforceandroid;

class MainAttack {
    private static String protocol;
    private static String userFile;
    private static String passFile;
    private static int threadCount;
    private static String URLorIP;
    private static UserPassFetcher userPassFetcher;
    private static AttackEngine engine;
    private static MyLogger logger;
    private String accessDeniedString;
    private String usernameItem;
    private String passwordItem;

    public MainAttack(String protocol, String userFile, String passFile, String URLorIP,
                      int threadCount, MyLogger logger, String usernameItem, String passwordItem, String accessDeniedString) {
        this.protocol = protocol;
        this.userFile = userFile;
        this.passFile = passFile;
        this.URLorIP = URLorIP;
        this.threadCount = threadCount;
        this.logger = logger;
        this.accessDeniedString = accessDeniedString;
        this.usernameItem = usernameItem;
        this.passwordItem = passwordItem;
    }

    public void startAttack() {

        try {
            logger.d("Loading user and pass list to memory");
            userPassFetcher = new UserPassFetcher(userFile, passFile);
            userPassFetcher.loadAllUsersAndPasswords();
            engine = new AttackEngine(userPassFetcher,
                    protocol, URLorIP, threadCount, logger, usernameItem, passwordItem, accessDeniedString);
        } catch (Exception e) {
            e.printStackTrace();
            logger.d(e.getMessage());
            logger.m("Connection failed");
            return;
        }
        try {
            engine.attack();
        } catch (Exception e) {
            logger.d(e.getMessage());
            logger.m("Connection failed");
            System.err.println(e.getMessage());
            return;
        }
        if (engine.haveFoundCredentials()) {
            logger.m("Credentials found:\nUser: " + engine.getCredentials()[0]
                    + "\nPassword: " + engine.getCredentials()[1]);
        } else
            logger.m("Credentials not in list");
    }
}
