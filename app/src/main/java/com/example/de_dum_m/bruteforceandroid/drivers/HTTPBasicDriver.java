package com.example.de_dum_m.bruteforceandroid.drivers;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.URI;

/**
 * Created by de-dum_m on 29/09/14.
 */
public class HTTPBasicDriver extends Driver {
    private AuthScope authScope;
    private DefaultHttpClient httpclient;
    private String URL;

    public int connect(String URLorIP) {
        authScope = new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT);
        this.URL = URLorIP;
        return 1;
    }

    public int tryCredentials(String username, String password) throws Exception {
        httpclient = new DefaultHttpClient();
        int statusCode = 401;
        try {
            httpclient.getCredentialsProvider().setCredentials(
                    authScope,
                    new UsernamePasswordCredentials(username, password));
            HttpGet request = new HttpGet();
            request.setURI(new URI(URL));
            UsernamePasswordCredentials credentials =
                    new UsernamePasswordCredentials(username, password);
            BasicScheme scheme = new BasicScheme();
            Header authorizationHeader = scheme.authenticate(credentials, request);
            request.addHeader(authorizationHeader);

            HttpResponse response = httpclient.execute(request);
            statusCode = response.getStatusLine().getStatusCode();

        } catch(IOException e) {
            if (!e.getMessage().equals("Request aborted"))
                throw new Exception(e);

        }
        return statusCode;
    }

    public void disconnect() {
        httpclient.getConnectionManager().shutdown();
    }
}
